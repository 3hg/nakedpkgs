# nakeDeb packages sources

les sources nakedpkgs contiennent de quoi construire les paquets debian intégrés à [nakeDeb](https://nakedeb.arpinux.org)

ces paquets sont disponibles depuis le [dépôt nakeDeb](https://nakedeb.arpinux.org/repo)

## contents

* [bashmount](bashmount) : utilitaire de gestion de volumes internes ou amovibles
* [clarity-icon-theme](clarity-icon-theme) : thème d'icônes pour nakeDeb
* [dracula-gtk-theme](dracula-gtk-theme) : thème gtk sombre pour nakeDeb
* [eyecandy](eyecandy) : utilitaire de gestion pour picom (ombres et transparences)
* [fluxbox-automenu](fluxbox-automenu) : un menu auto-généré pour fluxbox par @prx
* [info4help](info4help) : outil d'information système avec envoi de rapport préformaté pour les forums par @3hg
* [nakedbase](nakedbase) : version, GRUB config et fichiers système pour nakeDeb
* [nakedconky](nakedconky) : différentes configurations pour conky et un menu pour en changer
* [nakeddots](nakeddots) : fichiers cachés de l'utilisateur sur nakedeb
* [nakedfluxbox](nakedfluxbox) : configuration de fluxbox sur nakeDeb
* [nakedhelp](nakedhelp) : centre d'aide pour nakeDeb (intégré et [en ligne](https://nakedeb.arpinux.org/wiki))
* [nakedi3wm](nakedi3wm) : configuration de i3wm sur nakeDeb
* [nakedlook](nakedlook) : nakeDeb artwork
* [nakedsway](nakedsway) : configuration de sway sur nakeDeb
* [nakedtools](nakedtools) : petits outils pour nakeDeb
* [nordic-gtk-theme](nordic-gtk-theme) : thème gtk Nord
* [nordzy-dark-cursor](nordzy-dark-cursor) : thème curseur Nord
* [nordzy-green-dark](nordzy-green-dark) : thème d'icônes Nord square
* [tkmenu](tkmenu) : menu graphique rapide et léger par @prx
* [zafiro-nord-dark-icon-theme](zafiro-nord-dark-icon-theme) : thème d'icônes Nord circle
