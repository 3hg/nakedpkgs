#!/usr/bin/env python3
# -*- coding:Utf-8 -*- 

"""
Developers :  thuban (thuban@yeuxdelibad.net)
              arpinux (arpinux@member.fsf.org)
              Starsheep (starsheep@openmailbox.org)
              FirePowi
              Michael Gebetsroither <michael@mgeb.org> (paste part)

Licence :     GNU General Public Licence v3

Description : Tool to list a description of the system
              to improve help on forums in case of trouble.
"""

import sys, os, getopt, gettext
from ui.cli import cli
from ui.web import web

appcmd = "info4help"

#i18n
gettext.bindtextdomain(appcmd, '/usr/share/locale')
gettext.textdomain(appcmd)
_ = gettext.gettext

########################################################################
# Help message
########################################################################

def print_help():
    print(_("Usage: ")+appcmd+_(" <arguments>"))
    print(_("Argument list:"))
    print(_("-w or --web -> Web graphical user interface"))
    print(_("-t <file> or --template=<file> -> Template file for export"))
    print(_("-h or --help -> Help"))

########################################################################
# Argument parsing and application execution
########################################################################

def main(argv):
    # Init variables
    template = ""
    web_interface = False
    cli_mode = "0"
    # Check argument avaibility
    try:
        opts, args = getopt.getopt(argv,"ht:ws:",["help", "template=", "web", "silent="])
    except getopt.GetoptError:
        print(_("Wrong argument!"))
        print_help()
        sys.exit(2)
    # Read arguments
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print_help()
            sys.exit(2)
        if opt in ("-t", "--template"):
            template = arg
            # Check path
            if not os.path.isfile(template):
                print(_("Export template not available! "), template)
                print_help()
                sys.exit(2)
        if opt in ("-w", "--web"):
            web_interface = True
        if opt in ("-s", "--silent"):
            cli_mode = arg
    # Start application
    if web_interface:
        web(template)
    else:
        cli(template, cli_mode)
    sys.exit()

if __name__ == '__main__':
    main(sys.argv[1:])

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
