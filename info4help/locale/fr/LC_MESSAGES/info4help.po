# Traduction pour info4help.
# Copyright (C) 2021
# This file is distributed under the same license as the info4help package.
# 3hg trad team <3hgadmins@3hg.fr>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: 1.3\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-07-15 18:04+0200\n"
"PO-Revision-Date: 2021-07-15-16:30+CET\n"
"Last-Translator: 3hg trad team <3hgadmins@3hg.fr>\n"
"Language-Team: French\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: lib/export.py:63
msgid ""
"Failed to uplad code to pastebin: \n"
"{}"
msgstr ""
"Echec de l'envoi du code sur le pastebin : \n"
"{}"

#: lib/cmd.py:109
msgid "Your Debian version"
msgstr "Votre version de Debian"

#: lib/cmd.py:110
msgid "Name of the host"
msgstr "Nom de l'hôte système"

#: lib/cmd.py:111
msgid "Kernel version"
msgstr "Version du noyau"

#: lib/cmd.py:113
msgid "Pinning preferences"
msgstr "Préférences des dépôts APT"

#: lib/cmd.py:116
msgid "List of internal peripherals"
msgstr "Liste des périphériques internes"

#: lib/cmd.py:117
msgid "List of connected peripherals"
msgstr "Liste des périphériques connectés"

#: lib/cmd.py:119
msgid "Hardware informations"
msgstr "Informations sur le matériel"

#: lib/cmd.py:120
msgid "Monitor configuration"
msgstr "Configuration de l'écran"

#: lib/cmd.py:124
msgid "List of network interfaces"
msgstr "Liste des interfaces réseau"

#: lib/cmd.py:126
msgid "Configuration of the DNS resolver"
msgstr "Configuration du résolveur DNS"

#: lib/cmd.py:127
msgid "Network configuration"
msgstr "Configuration du réseau"

#: lib/cmd.py:128
msgid "IP routing table"
msgstr "Table de routage IP"

#: lib/cmd.py:131
msgid "Disk use"
msgstr "Utilisation du disque"

#: lib/cmd.py:133
msgid "Disk partitioning"
msgstr "Partitionnement du disque"

#: lib/cmd.py:134
msgid "File system table"
msgstr "Table de montage système"

#: info4help.py:33
msgid "Usage: "
msgstr "Utilisation : "

#: info4help.py:33
msgid " <arguments>"
msgstr " <arguments>"

#: info4help.py:34
msgid "Argument list:"
msgstr "Liste des arguments :"

#: info4help.py:35
msgid "-w or --web -> Web graphical user interface"
msgstr "-w ou --web -> Interface utilisateur HTML"

#: info4help.py:36
msgid "-t <file> or --template=<file> -> Template file for export"
msgstr "-t <fichier> ou --template=<fichier> -> Modèle d'export du fichier"

#: info4help.py:37
msgid "-h or --help -> Help"
msgstr "-h ou --help -> Aide"

#: info4help.py:52
msgid "Wrong argument!"
msgstr "Mauvais argument !"

#: info4help.py:64
msgid "Export template not available! "
msgstr "Modèle d'export invalide ! "

#: ui/web.py:80
msgid ""
"The following infomation are already formatted. All you need to do is to "
"copy/paste in your forum message."
msgstr ""
"Les informations présentées sont déjà mises en forme. Tout ce qu'il vous reste à "
"faire est de les copier/coller dans votre message sur le forum."

#: ui/web.py:82
msgid ""
"You can also upload this information on a pastebin by clicking on the "
"following button:"
msgstr ""
"Vous pouvez aussi envoyer ces informations sur un pastebin debian :"

#: ui/web.py:84
msgid "Upload on paste.debian.net"
msgstr "Envoyer sur paste.debian.net"

#: ui/web.py:86
msgid "Info4Help report"
msgstr "Rapport info4help"

#: ui/web.py:87
msgid "Installation"
msgstr "Installation"

#: ui/web.py:88
msgid "Hardware"
msgstr "Matériel"

#: ui/web.py:89
msgid "Network"
msgstr "Réseau"

#: ui/web.py:90
msgid "Storage"
msgstr "Stockage"

#: ui/web.py:91
msgid "Complete report"
msgstr "Rapport complet"

#: ui/cli.py:44
msgid ""
"This application obtains information about your Debian GNU/Linux system to "
"get help on forums."
msgstr ""
"Cette application récolte des informations à propos de votre système Debian "
"GNU/Linux afin d'obtenir de l'aide sur les forums."

#: ui/cli.py:46
msgid "The answer is pre-formatted, you can easily copy/paste it on forums."
msgstr "Le texte sera pré-formaté, il vous suffit de la coller sur un forum."

#: ui/cli.py:48
msgid ""
"You can also provide the pastebin link (if asked): it points on the report."
msgstr ""
"Vous pouvez aussi donner le lien qui sera affiché, il pointe vers le compte-rendu."

#: ui/cli.py:70
msgid "What information are you interested in?"
msgstr "Quelle(s) information(s) désirez-vous obtenir ?"

#: ui/cli.py:73
msgid "information on the system"
msgstr "informations à propos de votre système"

#: ui/cli.py:75
msgid "information on the hardware"
msgstr "informations à propos de votre matériel"

#: ui/cli.py:77
msgid "information on the network"
msgstr "informations à propos du réseau"

#: ui/cli.py:79
msgid "information on the storage"
msgstr "informations à propos de vos disques"

#: ui/cli.py:81
msgid "all information listed above"
msgstr "rapport complet"

#: ui/cli.py:83
msgid "Confirm with the Enter key"
msgstr "Confirmez avec la touche Entrée"

#: ui/cli.py:98
msgid "Unknown choice"
msgstr "Choix unconnu"

#: ui/cli.py:103
msgid "Please find below the report to copy/paste on forums"
msgstr "Veuillez trouver ci-dessous le rapport à copier/coller sur le forum"

#: ui/cli.py:110
msgid ""
"Would you like upload the report on internet and obtain the URL address ? "
"(on a pastebin) [y/n]"
msgstr ""
"Voulez-vous envoyer ce rapport sur un pastebin et obtenir le lien ? [o/n]"

#: ui/cli.py:113
msgid "yes"
msgstr "oui"

#: ui/cli.py:117
msgid "Copy this URL address below which links to your report on internet:"
msgstr "Copiez ce lien qui contient le rapport d'informations :"

#: ui/cli.py:122
msgid "Press Enter to exit..."
msgstr "Pressez Entrée pour quitter..."
