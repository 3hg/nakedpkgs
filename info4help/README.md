info4help
=========

Utilitaire listant les informations du système Debian GNU/Linux afin de les poster sur un forum pour obtenir de l'aide.

info4help formate les informations en bbcode ou markdown au choix.

traduction :
------------

    xgettext $(find . -name "*.py") -o locale/info4help.pot
    cp locale/info4help.pot locale/fr/LC_MESSAGES/info4help.po

traduire le fichier '.po' puis le convertir en '.mo'

    cd locale/fr/LC_MESSAGES/
    msgfmt -o info4help.mo -v info4help.po

construction, test & install :
------------------------------

    $ equivs-build info4help.equivs
    $ lintian info4help-$VERSION.deb
    # dpkg -i info4help-$VERSION.deb
